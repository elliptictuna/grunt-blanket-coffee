/*
 * grunt-blanket
 * https://github.com/alex-seville/grunt-blanket
 *
 * Copyright (c) 2013 alex-seville
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    jshint: {
      all: [
        'Gruntfile.js',
        'tasks/*.js',
      ],
      options: {
        jshintrc: '.jshintrc',
      },
    },

    coffeelint: {
      all: [
        'Gruntfile.coffee',
        'tasks/*.coffee',
        'tasks/*.litcoffee',
        'tasks/*.iced',
        'tasks/*.iced.md',
      ],
      options: {
        configfile: 'coffeelint.json'
      }

    },

    // Configuration to be run (and then tested).
    blanket: {
      instrument: {
        options: {
          debug: true,
          extensions: ['.js', '.coffee', '.iced', '.md', '.litcoffee']
        },
        files: {
          'cov/': ['tasks/'],
        },
      }
    },

    

  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-coffeelint');
 

  // By default, lint and run all tests.
  grunt.registerTask('default', ['jshint','blanket']);



};
